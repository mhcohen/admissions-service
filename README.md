# Admissions service 

This is a simple CRUD service to manage patient admissions.
Admissions are stored in an in memroy database, only providing persistance within a given run of the application.

## Admission JSON

{
    "id" : int,
    "dateOfAdmission" : string,
    "name" : string,
    "birthDate" : string,
    "sex" : string,
    "category" : string,
    "source" : string
}

id:
a generated value and should not be supplied upon admission creation.
Cannot be updated once set.

dateOfAdmission:
string representation of a datetime. a generated value and should not be supplied upon admission creation.
Cannot be updated once set.

name:
name of patient being admitted.

birthDate:
string representation of a date. date of birth of patient being admitted.
Cannot be a future date.

sex:
string representation of an enumerated type.
Valid values are: FEMALE, MALE, INTERSEX, UNKNOWN.

category:
string representation of an enumerated type.
Valid values are: NORMAL, INPATIENT, EMERGENCY, OUTPATIENT.

source:
originating system of the admission.
Cannot be updated once set.

## API
API endpoint root: http://localhost:8080/api/v1/admissions

curl --header "Content-Type: application/json" \
    --request GET \
    http://localhost:8080/api/v1/admissions/
returns all the admissions in the system

curl --header "Content-Type: application/json" \
    --request GET \
    http://localhost:8080/api/v1/admissions/<<id>>
returns the admission with matching <<id>>

curl --header "Content-Type: application/json" \
    --request POST \
    --data '{"name": "Patient Name", "birthDate": "2000-12-31", "sex": "UNKNOWN", "category": "NORMAL", "source": "REST CALL"}' \
    http://localhost:8080/api/v1/admissions/
adds the supplied admission to the system

curl --header "Content-Type: application/json" \
    --request PUT \
    --data '{"id": <<id>>, "name": "Patient Name", "birthDate": "2000-12-31", "sex": "UNKNOWN", "category": "NORMAL"}' \
    http://localhost:8080/api/v1/admissions/<<id>>
updates the name, birhtDate, sex and category of the specified admission

curl --header "Content-Type: application/json" \
    --request DELETE \
    http://localhost:8080/api/v1/admissions/<<id>>
deletes the admission with matching <<id>>