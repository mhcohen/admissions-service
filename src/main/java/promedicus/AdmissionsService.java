package promedicus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdmissionsService {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(AdmissionsService.class);

	public static void main(String[] args) {
		SpringApplication.run(AdmissionsService.class, args);
	}

}
