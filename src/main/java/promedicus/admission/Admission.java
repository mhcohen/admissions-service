package promedicus.admission;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.commons.lang3.StringUtils;

import promedicus.enums.Category;
import promedicus.enums.Sex;

@Entity
public class Admission {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id = -1;
	
	@Column(nullable = false)
	private LocalDateTime dateOfAdmission;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private LocalDate birthDate;
	
	@Column(nullable = false)
	private Sex sex;
	
	@Column(nullable = false)
	private Category category;
	
	@Column(nullable = false)
	private String source;
	
	protected Admission() {
		
	}
	
	public Admission(LocalDateTime dateOfAdmission, String name, LocalDate birthDate, String sex, String category, String source) {
		this.dateOfAdmission = dateOfAdmission;
		this.name = name;
		this.birthDate = birthDate;
		this.sex = Sex.valueOf(sex);
		this.category = Category.valueOf(category);
		this.source = source;
	}
	
	public Admission updateFrom(Admission updaterAdmission) {
		if(StringUtils.isNotBlank(updaterAdmission.getName()))
			this.name = updaterAdmission.getName();
		
		if(updaterAdmission.birthDate != null)
			this.birthDate = updaterAdmission.birthDate;
		
		if(updaterAdmission.sex != null)
			this.sex = updaterAdmission.sex;
		
		if(updaterAdmission.category != null)
			this.category = updaterAdmission.category;
			
		return this;
	}
	
	@Override
	public String toString() {
		return String.format("Admission[id=%d, dateOfAdmission=%s, name=%s, birthDate=%s, sex=%s, category=%s, source=%s]", 
				id,
				dateOfAdmission != null ? dateOfAdmission.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")): null,
				name,
				birthDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
				sex,
				category,
				source);
	}

	public long getId() {
		return id;
	}

	public LocalDateTime getDateOfAdmission() {
		return dateOfAdmission;
	}

	public String getName() {
		return name;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public Sex getSex() {
		return sex;
	}

	public Category getCategory() {
		return category;
	}

	public String getSource() {
		return source;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setDateOfAdmission(LocalDateTime dateOfAdmission) {
		this.dateOfAdmission = dateOfAdmission;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setSource(String source) {
		this.source = source;
	}

}
