package promedicus.admission;

import org.springframework.data.repository.CrudRepository;

public interface AdmissionRepository extends CrudRepository<Admission, Long> {
	
}
