package promedicus.admission;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;

import promedicus.exceptions.InvalidDateException;

public class AdmissionValidator {
	
	private AdmissionValidator () {}
	
	public static boolean validate(Admission admission) {
		
		if(admission.getBirthDate() == null) {
			throw new InvalidDateException("Attempted to update or create admission with empty birthdate");
		}
		
		if(LocalDate.now().isBefore(admission.getBirthDate())) {
			throw new InvalidDateException(String.format(
					"Attempted to update or create admission with illegal birthDate %s : birthDate cannot be in the future",
					admission.getBirthDate().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))));
		}
		
		if(StringUtils.isBlank(admission.getName())) {
			throw new IllegalArgumentException("Attempted to update existing admission with empty name");
		}
		
		return true;
	}
}
