package promedicus.admission;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import promedicus.exceptions.AdmissionIdMismatchException;
import promedicus.exceptions.AdmissionNotFoundException;

@RestController
@RequestMapping("/api/v1")
public class AdmissionsController {

	private static final Logger log = LoggerFactory.getLogger(AdmissionsController.class);

	@Autowired
	private AdmissionRepository admissionRepository;

	@GetMapping("/admissions")
	public Iterable<Admission> getAllAdmissions() {
		if(log.isInfoEnabled())
			log.info("Attempting to retrieve all admissions");
		
		return admissionRepository.findAll();
	}

	@GetMapping("/admissions/{id}")
	public Admission getAdmission(@PathVariable("id") Long id) {
		if(log.isInfoEnabled())
			log.info("Attempting to retrieve admission with id = " + id);
		
		return admissionRepository.findById(id).orElseThrow(AdmissionNotFoundException::new);
	}

	@PostMapping("/admissions")
	@ResponseStatus(HttpStatus.CREATED)
	public Admission addAdmission(@RequestBody Admission admission) {
		admission.setDateOfAdmission(LocalDateTime.now());
		
		if(log.isInfoEnabled())
			log.info("Attempting to add new admission " + admission.toString());
		
		AdmissionValidator.validate(admission);
		
		return admissionRepository.save(admission);
	}

	@PutMapping("/admissions/{id}")
	public Admission updateAdmission(@RequestBody Admission admission, @PathVariable("id") Long id) {
		if(log.isInfoEnabled())
			log.info("Attempting to update existing admission with id = " + id + " to " + admission.toString());
		
		if(admission.getId() != id) {
			throw new AdmissionIdMismatchException(String
					.format("Attempted to update existing admission %d with content for id %d", admission.getId(), id));
		}
		
		AdmissionValidator.validate(admission);
		
		Admission original = admissionRepository.findById(id).orElseThrow(AdmissionNotFoundException::new);
		return admissionRepository.save(original.updateFrom(admission));
	}

	@DeleteMapping("/admissions/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public @ResponseBody void deleteAdmission(@PathVariable("id") Long id) {
		if(log.isInfoEnabled())
			log.info("Attempting to delete admission with id = " + id);
		
		Admission admission = admissionRepository.findById(id).orElseThrow(AdmissionNotFoundException::new);
		admissionRepository.delete(admission);
	}

}
