package promedicus.enums;

public enum Category {
	NORMAL,
	INPATIENT,
	EMERGENCY,
	OUTPATIENT;
}
