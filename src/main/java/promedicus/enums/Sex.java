package promedicus.enums;

public enum Sex {
	UNKNOWN,
	FEMALE,
	MALE,
	INTERSEX;
}
