package promedicus.exceptions;

public class AdmissionIdMismatchException extends IllegalStateException {
	
	private static final long serialVersionUID = 1L;
	
	public AdmissionIdMismatchException(String message) {
		super(message);
	}

}
