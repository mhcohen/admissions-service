package promedicus.exceptions;

public class InvalidDateException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;
	
	public InvalidDateException(String message) {
		super(message);
	}
}
